#!/bin/bash
VERSION=${VERSION:-0.4}
# remove spaces from file names

## GPLv3
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

CWD=$(pwd)

help() {
    ## this function provides help if the user clearly does
    ## not know how to use the application

    echo "despace [commands] <arguments>"
    echo ""
    echo "despace foo\ bar            = remove space from filename 'foo bar'"
    echo "despace foo\ bar baz\ quux  = remove spaces from files 'foo bar' and 'baz\ quux'"
    echo "despace *                   = remove spaces from all files in current directory"
    echo "despace /path/to/dir/*      = remove spaces from all files in /path/to/dir"
    echo "despace --swap '_' foo\ bar = replaces space with underscore in filename 'foo bar'"
    echo ""
    echo "See man page for more information"
    echo ""
}


### user interactions below
while [ True ]; do
if [ "$1" = "swap" -o "$1" = "--swap" -o "$1" = "-s" -o "$1" = "s" ]; then 
    SWAP=1
    SWAPPER=$1
    shift 2
elif [ "$1" = "dryrun" -o "$1" == "--dry-run" -o "$1" = "-d" -o "$1" = "d" ]; then 
    FAKE=1
    shift 1
elif [ "$1" = "tidy" -o "$1" == "--tidy" -o "$1" = "-t" -o "$1" = "t" ]; then 
    TIDY=1
    shift 1
elif [ "$1" = "help" -o "$1" = "--help" -o "$1" = "-h" -o "$1" = "h" ]; then 
    help
    shift 1
elif [ "$1" = "version" -o "$1" = "--version" -o "$1" = "-v" -o "$1" = "v" ]; then     
    echo "despace version $VERSION"
    exit
else
    break
fi
done

# everything else left at this point
# should be filenames we want to alter
ARG=("${@}")
ARRAYSIZE=${#ARG[*]}

###### Main Loop

if [ "X$ARRAYSIZE" == "X0" ]; then
   help
   exit
fi

while [ True ]; do
    for item in "${ARG[@]}"; do

    if [ "$SWAP" == 1 ]; then
	if [ "$FAKE" == 1 ]; then
            echo "${item}" " -> " `echo "${item}" | tr " " "${SWAPPER}"`
	else
	    /usr/bin/mv -i "${item}" `echo "${item}" | tr " " "${SWAPPER}"` 2>/dev/null
	fi

    else
	if [ "$FAKE" == 1 ]; then
	    echo "${item}" " -> " `echo "${item}" | tr -d " "`
	else
	    /usr/bin/mv -i "${item}" `echo "${item}" | tr -d " "` 2>/dev/null
	fi

    fi

    if [ "$TIDY" == 1 ]; then
	if [ "$FAKE" == 1 ]; then
	    echo "${item}" " -> " `echo "${item}" | sed "s/[',;:=/\"]/-/g" `
	else
	   /usr/bin/mv -i "${item}" `dirname "${item}"`/`echo $(basename "${item}") | sed "s/[',;:=/\"]/-/g"` 
	fi
    fi

    done
exit
done

